import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RedditService } from './services/reddit.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatPaginatorModule
} from '@angular/material';
import { NgMatSearchBarModule } from 'ng-mat-search-bar';

import { AppComponent } from './app.component';
import { SubRedditFeedComponent } from './components/sub-reddit-feed/sub-reddit-feed.component';
import { SubRedditPostComponent } from './components/sub-reddit-post/sub-reddit-post.component';
import { SubRedditPostDetailsComponent } from './components/sub-reddit-post-details/sub-reddit-post-details.component';

@NgModule({
  declarations: [
    AppComponent,
    SubRedditFeedComponent,
    SubRedditPostComponent,
    SubRedditPostDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatPaginatorModule,
    NgMatSearchBarModule,
    AppRoutingModule
  ],
  providers: [RedditService],
  bootstrap: [AppComponent]
})
export class AppModule { }
