import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  MatCardModule,
  MatButtonModule,
  MatIconModule
} from '@angular/material';
import { RedditPost } from '../../models/reddit-post';
import { SubRedditPostComponent } from './sub-reddit-post.component';

describe('SubRedditPostComponent', () => {
  let component: SubRedditPostComponent;
  let fixture: ComponentFixture<SubRedditPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [ SubRedditPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubRedditPostComponent);
    component = fixture.componentInstance;
    component.post = new RedditPost('thumb.jpg', '242342', 32, 'auth', 342, 'asdad.com', 'title', 'mainText');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
