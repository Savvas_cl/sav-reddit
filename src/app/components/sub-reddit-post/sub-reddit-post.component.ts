import { Component, OnInit, Input } from '@angular/core';
import { RedditPost } from '../../models/reddit-post';

@Component({
  selector: 'app-sub-reddit-post',
  templateUrl: './sub-reddit-post.component.html',
  styleUrls: ['./sub-reddit-post.component.scss']
})
export class SubRedditPostComponent implements OnInit {
  @Input() post: RedditPost;

  constructor() {

  }

  ngOnInit() {
  }

}
