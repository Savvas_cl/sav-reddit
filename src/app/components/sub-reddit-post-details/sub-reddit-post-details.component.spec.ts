import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatListModule
} from '@angular/material';
import { SubRedditPostDetailsComponent } from './sub-reddit-post-details.component';

describe('SubRedditPostDetailsComponent', () => {
  let component: SubRedditPostDetailsComponent;
  let fixture: ComponentFixture<SubRedditPostDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatListModule
      ],
      declarations: [ SubRedditPostDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubRedditPostDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
