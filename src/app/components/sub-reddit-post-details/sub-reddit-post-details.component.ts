import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RedditService } from '../../services/reddit.service';
import { RedditPost } from '../../models/reddit-post';

@Component({
  selector: 'app-sub-reddit-post-details',
  templateUrl: './sub-reddit-post-details.component.html',
  styleUrls: ['./sub-reddit-post-details.component.scss']
})
export class SubRedditPostDetailsComponent implements OnInit {
  public postPermalink: string;
  public postDetails: RedditPost;
  public postComments: any;
  public isLoaded: boolean;


  constructor(public activatedRoute: ActivatedRoute, private redditApi: RedditService) {
    this.isLoaded = false;
  }

  ngOnInit() {
    this.postPermalink = this.activatedRoute.snapshot.paramMap.get('permalink');

    this.redditApi.getSubredditPostDetails(this.postPermalink).subscribe(
      post => {
        this.postDetails = post.postDetails;
        this.postComments = post.postComments;
        this.isLoaded = true;
      },
      err => {
        console.error(err);
      }
    );
  }

}
