import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatPaginatorModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule
} from '@angular/material';
import { SubRedditPostComponent } from '../sub-reddit-post/sub-reddit-post.component';
import { SubRedditFeedComponent } from './sub-reddit-feed.component';

describe('SubRedditFeedComponent', () => {
  let component: SubRedditFeedComponent;
  let fixture: ComponentFixture<SubRedditFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatPaginatorModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule
      ],
      declarations: [
        SubRedditFeedComponent,
        SubRedditPostComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubRedditFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
