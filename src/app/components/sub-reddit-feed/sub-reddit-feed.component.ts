import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';
import { RedditService } from '../../services/reddit.service';
import { RedditPost } from '../../models/reddit-post';
import { RedditPostsList } from '../../models/reddit-posts-list';

@Component({
  selector: 'app-sub-reddit-feed',
  templateUrl: './sub-reddit-feed.component.html',
  styleUrls: ['./sub-reddit-feed.component.scss']
})
export class SubRedditFeedComponent implements OnInit {
  public subRedditPosts: RedditPost[];
  public subRedditList: RedditPostsList;
  public error: boolean;
  public subRedditName: string;
  public length: number;
  public pageSize: number;
  public pageIndex: number;
  public pageSizeOptions: number[] = [5, 10, 25];

  constructor(private redditApi: RedditService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.paramMap.subscribe(params => {
        this.subRedditName = `${params.get('reddit')}.json`;
    });
    this.error = false;
    this.length = 1000;
    this.pageSize = 10;
    this.pageIndex = 0;
  }

  ngOnInit() {
    this.subRedditListSubscribe(this.subRedditName, this.pageSize);
  }

  updateListData(event: PageEvent) {
    if (this.pageSize !== event.pageSize) {
      this.subRedditListSubscribe(this.subRedditName, event.pageSize);
      this.pageSize = event.pageSize;
    }
    if (this.pageIndex !== event.pageIndex) {
      if ( event.pageIndex > this.pageIndex ) {
        this.subRedditListSubscribe(this.subRedditName, event.pageSize, this.subRedditList.nextPageCode);
      } else {
        this.subRedditListSubscribe(this.subRedditName, event.pageSize, undefined, this.subRedditList.previousPageCode);
      }
      this.pageIndex = event.pageIndex;
    }
  }

  subRedditListSubscribe(subReddit: string, numberOfPosts: number, after?: string, before?: string) {
    this.redditApi.getSubredditPostsList(subReddit, numberOfPosts, after, before).subscribe(
      redditPostsList => {
        this.subRedditPosts = redditPostsList.redditPosts;
        this.subRedditList = redditPostsList;
        this.error = false;
      },
      err => {
        console.error(err);
        this.error = true;
      }
    );
  }

}
