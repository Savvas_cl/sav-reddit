import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RedditPost } from '../models/reddit-post';
import { RedditPostsList } from '../models/reddit-posts-list';

@Injectable({
  providedIn: 'root'
})
export class RedditService {
  private readonly baseApiUrl = 'https://www.reddit.com/';
  private readonly noImage = 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/1280px-No_image_3x4.svg.png';

  constructor(private http: HttpClient) {

  }

  getSubredditPostsList(subReddit: string, numberOfPosts: number, after?: string, before?: string): Observable<RedditPostsList> {

    let requestUrl: string;
    if (after) {
      requestUrl = `${this.baseApiUrl}r/${subReddit}?limit=${numberOfPosts}&count=100&after=${after}`;
    } else if (before) {
      requestUrl = `${this.baseApiUrl}r/${subReddit}?limit=${numberOfPosts}&count=100&before=${before}`;
    } else {
     requestUrl = `${this.baseApiUrl}r/${subReddit}?limit=${numberOfPosts}`;
    }

    return this.http.get<any>(requestUrl)
      .pipe(
        map(posts => {
          return {
            redditPosts: posts.data.children.map(post => {
              return new RedditPost(
                post.data.thumbnail !== 'self' ? post.data.thumbnail : this.noImage,
                post.data.created,
                post.data.num_comments,
                post.data.author,
                post.data.score,
                post.data.permalink,
                post.data.title,
                post.data.selftext
              );
            }),
            nextPageCode: posts.data.after,
            previousPageCode: posts.data.before
          };
        })
      );
  }


  getSubredditPostDetails(postPermalink: string): Observable<any> {
    const requestUrl = `${this.baseApiUrl}${postPermalink}.json`;

    return this.http.get<any>(requestUrl)
      .pipe(
        map(postDetails => {
          const details = postDetails[0].data.children[0].data;
          const comments = postDetails[1].data.children;
          return {
            postDetails: new RedditPost(
                details.thumbnail !== 'self' ? details.thumbnail : this.noImage,
                details.created,
                details.num_comments,
                details.author,
                details.score,
                details.permalink,
                details.title,
                details.selftext
            ),
            postComments: comments
          };
        })
      );
  }
}
