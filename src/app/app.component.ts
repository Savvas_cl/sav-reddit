import { Component, ComponentRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'sav-reddit';
  public search: FormControl = new FormControl('');
  public searchTerm = '';
  public componentRef;

  constructor(private route: Router) {}

  searchSubReddit(event: Event) {
    this.search.setValue('');
    this.componentRef.searchTerm = event;
    if (this.componentRef.subRedditListSubscribe) {
      this.componentRef.subRedditListSubscribe(event + '.json', this.componentRef.pageSize);
      this.route.navigate(['/sub-reddit-feed', event]);
    } else {
      this.route.navigate(['/sub-reddit-feed', 'sweden']);
    }
  }

  onActivate(componentRef) {
    this.componentRef = componentRef;
  }
}
