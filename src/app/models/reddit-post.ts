export class RedditPost {

  constructor(public thumbnail: string,
              public created: string,
              public numberOfComments: number,
              public author: string,
              public score: number,
              public permalink: string,
              public title: string,
              public selftext: string) {}
}
