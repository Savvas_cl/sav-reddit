import { RedditPost } from './reddit-post';

export class RedditPostsList {

  constructor(
    public nextPageCode: string,
    public previousPageCode: string,
    public redditPosts: RedditPost[] ) {

  }
}
