import { RedditPostsList } from './reddit-posts-list';
import { RedditPost } from './reddit-post';

describe('SubRedditPostsList', () => {
  it('should create an instance', () => {
    expect(new RedditPostsList(
      'afterCode',
      'beforeCode',
      [ new RedditPost('thumb.jpg', '242342', 32, 'auth', 342, 'asdad.com', 'title', 'mainText')]
      )).toBeTruthy();
  });
});
