import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubRedditFeedComponent } from './components/sub-reddit-feed/sub-reddit-feed.component';
import { SubRedditPostDetailsComponent } from './components/sub-reddit-post-details/sub-reddit-post-details.component';

const routes: Routes = [
  {
    path: 'sub-reddit-feed/:reddit',
    component: SubRedditFeedComponent,
    data: { title: 'List of sub-reddit posts' }
  },
  {
    path: 'sub-reddit-feed/:reddit/sub-reddit-post-details/:permalink',
    component: SubRedditPostDetailsComponent,
    data: { title: 'Details of a sub-reddit post' }
  },
  {
    path: '',
    redirectTo: '/sub-reddit-feed/sweden',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
